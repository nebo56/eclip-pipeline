#!/bin/bash -l
#$ -l h_vmem=32G
#$ -l tmem=32G
#$ -l h_rt=32:0:0
#$ -j y
#$ -S /bin/bash

export PATH=/home/skgthab/Programs/bedtools2.22.1/bin:$PATH
export PATH=/home/skgthab/Programs/samtools-0.1.19:$PATH
export PATH=/home/skgthab/Programs/custom_scripts:$PATH
export PATH=/home/skgthab/Programs/Homer-v4.5/bin:$PATH
export PATH=/home/skgthab/Programs/weblogo.2.8.2:$PATH
export PATH=/home/skgthab/Programs/BEDOPSv2.4.2:$PATH
export PATH=/home/skgthab/Programs/fastx_toolkit_0.0.13:$PATH
export PATH=/home/skgthab/programs/cutadapt/build/scripts-2.7:$PATH
export PATH=/home/skgthab/programs/sambamba_v0.6.5_linux:$PATH

data1=$1        #first pair
data2=$2        #second pair
path=/SAN/neuroscience/TE/Nejc/CLIPo-mapping-eCLIP/
genome=/SAN/neuroscience/TE/Shared/GRCh37.p13/GRCh37.p13.genome.fa
index=/SAN/neuroscience/TE/Shared/GRCh37.p13/GRCh37.p13.genome.fa.fai
chr_GTF="/SAN/neuroscience/TE/Shared/GRCh37.p13/gencode.v19.annotation.gtf"
genome="GRCh37"
thread="8"
genome_dir="/SAN/neuroscience/TE/Shared/GRCh37.p13-STAR"

mkdir $path$data1$data2-STAR

############################
## Mapping and formating ###
############################

# fastqc


# rRNA & rRNA removal


# adapter removal
cutadapt -f fastq --match-read-wildcards --times 1 -e 0.1 -O 1 --quality-cutoff 6 -m 18 -a NNNNNAGATCGGAAGAGCACACGTCTGAACTCCAGTCAC -g CTTCCGATCTACAAGTT -g CTTCCGATCTTGGTCCT -A AACTTGTAGATCGGA -A AGGACCAAGATCGGA -A ACTTGTAGATCGGAA -A GGACCAAGATCGGAA -A CTTGTAGATCGGAAG -A GACCAAGATCGGAAG -A TTGTAGATCGGAAGA -A ACCAAGATCGGAAGA -A TGTAGATCGGAAGAG -A CCAAGATCGGAAGAG -A GTAGATCGGAAGAGC -A CAAGATCGGAAGAGC -A TAGATCGGAAGAGCG -A AAGATCGGAAGAGCG -A AGATCGGAAGAGCGT -A GATCGGAAGAGCGTC -A ATCGGAAGAGCGTCG -A TCGGAAGAGCGTCGT -A CGGAAGAGCGTCGTG -A GGAAGAGCGTCGTGT -o $path$data1-adapter_trimmed.fastq.gz -p $path$data2-adapter_trimmed.fastq.gz $path$data1 $path$data2 > $path$data1$data2.adapterTrim.metrics

# second adapter removal - double ligation 
cutadapt -f fastq --match-read-wildcards --times 1 -e 0.1 -O 5 --quality-cutoff 6 -m 18 -A AACTTGTAGATCGGA -A AGGACCAAGATCGGA -A ACTTGTAGATCGGAA -A GGACCAAGATCGGAA -A CTTGTAGATCGGAAG -A GACCAAGATCGGAAG -A TTGTAGATCGGAAGA -A ACCAAGATCGGAAGA -A TGTAGATCGGAAGAG -A CCAAGATCGGAAGAG -A GTAGATCGGAAGAGC -A CAAGATCGGAAGAGC -A TAGATCGGAAGAGCG -A AAGATCGGAAGAGCG -A AGATCGGAAGAGCGT -A GATCGGAAGAGCGTC -A ATCGGAAGAGCGTCG -A TCGGAAGAGCGTCGT -A CGGAAGAGCGTCGTG -A GGAAGAGCGTCGTGT -o $path$data1-adapter_trimmed.round2.fastq.gz -p $path$data2-adapter_trimmed.round2.fastq.gz $path$data1-adapter_trimmed.fastq.gz $path$data2-adapter_trimmed.fastq.gz > $path$data1$data2.adapterTrim.round2.metrics

# mapping to genome - single hit
#time /home/skgthab/programs/STAR-master/bin/Linux_x86_64/STAR --runMode alignReads --runThreadN $thread --genomeDir $genome_dir --readFilesCommand zcat --readFilesIn $path$data1-adapter_trimmed.round2.fastq.gz $path$data2-adapter_trimmed.round2.fastq.gz --outSAMunmapped Within --outFilterMultimapNmax 1 --outFilterMultimapScoreRange 1 --outFileNamePrefix $path$data1$data2-STAR/ --outSAMattributes All --outStd SAM --outFilterType BySJout --outReadsUnmapped Fastx --outFilterScoreMin 10 --outSAMattrRGline ID:foo --alignEndsType EndToEnd | samtools view -hu - | sambamba sort -t 8 -o $path$data1$data2-STAR/$data1$data2.bam /dev/stdin

time /home/skgthab/programs/STAR-master/bin/Linux_x86_64/STAR --runMode alignReads --runThreadN $thread --genomeDir $genome_dir --readFilesCommand zcat --readFilesIn $path$data1-adapter_trimmed.round2.fastq.gz $path$data2-adapter_trimmed.round2.fastq.gz --outSAMunmapped Within --outFilterMultimapNmax 1 --outFilterMultimapScoreRange 1 --outFileNamePrefix $path$data1$data2-STAR/ --outSAMattributes All --outStd BAM_SortedByCoordinate --outFilterType BySJout --outReadsUnmapped Fastx --outFilterScoreMin 10 --outSAMattrRGline ID:foo --alignEndsType EndToEnd

# SAM to BAM
samtools view -hSb $path$data1$data2-STAR/Aligned.out.sam > $path$data1$data2-STAR/$data1.$data2.bam
rm $path$data1$data2-STAR/Aligned.out.sam

# PCR duplicate removal
python ${path}scripts/barcode_collapse_pe.py --bam $path$data1$data2-STAR/$data1.$data2.bam --out_file $path$data1$data2-STAR/$data1.$data2.rmDup.bam --metrics_file $path$data1.$data2-STAR/$data1.$data2.rmDup.metrics

# remove low quality reads
samtools view -hb -q255 $path$data1$data2-STAR/$data1.$data2.rmDup.bam > $path$data1$data2-STAR/$data1.$data2.rmDup-q255.bam

# BAM to BED
bedtools bamtobed -bedpe -mate1 -i $path$data1$data2-STAR/$data1.$data2.rmDup-q255.bam | sort -k1,1 -k2,2n -k6,6 > $path$data1$data2-STAR/$data1.$data2.rmDup-q255.bed

# BAM to single-end BED format
python ${path}scripts/pairedBED2singleBED-3.py $path$data1$data2-STAR/$data1.$data2.rmDup-q255.bed $path$data1$data2-STAR/$data1.$data2.rmDup-q255-single_end.bed

# get x-link positions
python ${path}scripts/BEDtoXlink.py $path$data1$data2-STAR/$data1.$data2.rmDup-q255-single_end.bed $path$data1$data2-STAR/$data1.$data2.rmDup-q255-single_end-xl.tmp

# sum file for iCount
sort -k1,1 -k2,2n -k6,6 $path$data1$data2-STAR/$data1.$data2.rmDup-q255-single_end-xl.tmp > $path$data1$data2-STAR/$data1.$data2.rmDup-q255-single_end-xl.bed
rm $path$data1$data2-STAR/$data1.$data2.rmDup-q255-single_end-xl.tmp
python ${path}scripts/BEDsum-iCount.py $path$data1$data2-STAR/$data1.$data2.rmDup-q255-single_end-xl.bed $path$data1$data2-STAR/$data1.$data2.rmDup-q255-single_end-xl-sum.bed

# get junction reads in bed and bam format
samtools view $path$data1$data2-STAR/$data1.$data2.rmDup-q255.bam | awk '($6 ~ /N/)' | samtools view -bS -t $index - > $path$data1$data2-STAR/$data1.$data2.rmDup-q255-junction.bam

# print unique number of cDNAs
echo "Number of raw cDNAs (mate1 & mate2)" > $path$data1$data2-01.Mapping-REPORT.txt
$((`zcat $data1 | wc -l ` / 4)) >> $path$data1$data2-01.Mapping-REPORT.txt
$((`zcat $data2 | wc -l ` / 4)) >> $path$data1$data2-01.Mapping-REPORT.txt
echo "A: Unique number of paired-end (-q 255) cDNAs" >> $path$data1$data2-01.Mapping-REPORT.txt
samtools flagstat $path$data1$data2-STAR/$data1.$data2.rmDup-q255.bam >> $path$data1$data2-01.Mapping-REPORT.txt
echo "B: Unique number of paired-end junction (-q 255) cDNAs" >> $path$data1$data2-01.Mapping-REPORT.txt
samtools flagstat $path$data1$data2-STAR/$data1.$data2.rmDup-q255-junction.bam >> $path$data1$data2-01.Mapping-REPORT.txt

# compress
gzip $path$data1$data2-STAR/*.bed
