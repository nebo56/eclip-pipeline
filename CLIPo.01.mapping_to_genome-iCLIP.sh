#!/bin/bash -l
#$ -l h_vmem=32G
#$ -l tmem=32G
#$ -l h_rt=32:0:0
#$ -j y
#$ -S /bin/bash

export PATH=/home/skgthab/Programs/bedtools2.22.1/bin:$PATH
export PATH=/home/skgthab/Programs/samtools-0.1.19:$PATH
export PATH=/home/skgthab/Programs/custom_scripts:$PATH
export PATH=/home/skgthab/Programs/Homer-v4.5/bin:$PATH
export PATH=/home/skgthab/Programs/weblogo.2.8.2:$PATH
export PATH=/home/skgthab/Programs/BEDOPSv2.4.2:$PATH
export PATH=/home/skgthab/Programs/fastx_toolkit_0.0.13:$PATH
export PATH=/home/skgthab/programs/cutadapt/build/scripts-2.7:$PATH
export PATH=/home/skgthab/programs/sambamba_v0.6.5_linux:$PATH

data=$1        #iCLIP raw fastq file
path=/SAN/neuroscience/TE/Nejc/CLIPo-mapping-eCLIP/
genome=/SAN/neuroscience/TE/Shared/GRCh37.p13/GRCh37.p13.genome.fa
index=/SAN/neuroscience/TE/Shared/GRCh37.p13/GRCh37.p13.genome.fa.fai
chr_GTF="/SAN/neuroscience/TE/Shared/GRCh37.p13/gencode.v19.annotation.gtf"
genome="GRCh37"
thread="8"
genome_dir="/SAN/neuroscience/TE/Shared/GRCh37.p13-STAR"

mkdir $path$data-STAR

############################
## Mapping and formating ###
############################

# fastqc


# rRNA & rRNA removal


# adapter removal
cutadapt -f fastq --match-read-wildcards --times 1 -e 0.1 -O 1 --quality-cutoff 6 -m 18 -a AGATCGGAAG $path$data > ${path}${data}.adapterTrim.fastq.gz 2> $path$data.adapterTrim.metrics

# swap random barcodes to headers of fastq file
python ${path}scripts/swap_barcode_to_header-fastq.py ${path}${data}.adapterTrim.fastq.gz ${path}${data}.adapterTrim.barcodes.fastq.gz

# mapping to genome - single hit
time /home/skgthab/programs/STAR-master/bin/Linux_x86_64/STAR --runMode alignReads --runThreadN $thread --readFilesCommand zcat --genomeDir $genome_dir --readFilesIn ${path}${data}.adapterTrim.barcodes.fastq.gz --outSAMunmapped Within --outFilterMultimapNmax 1 --outFilterMultimapScoreRange 1 --outFileNamePrefix $path$data-STAR/ --outSAMattributes All --outStd BAM_SortedByCoordinate --outFilterType BySJout --outReadsUnmapped Fastx --outFilterScoreMin 10 --outSAMattrRGline ID:foo --alignEndsType EndToEnd

# SAM to BAM and low quality reads removal
samtools view -Shb -q255 $path$data-STAR/Aligned.out.sam > $path$data-STAR/$data-q255.bam
rm $path$data-STAR/Aligned.out.sam

# convert BAM to bed with PCR duplicate removal
bedtools bamtobed -i $path$data-STAR/$data-q255.bam | awk '{print $1 "\t" $2 "\t" $3 "\t\t" $4 "\t" $6}' | sort -k1,1 -k2,2n -k5,5 -k6,6 > $path$data-STAR/$data-q255.rmDup.bed

# get x-link positions
python ${path}scripts/BEDtoXlink.py $path$data-STAR/$data-q255.rmDup.bed $path$data-STAR/$data-q255.rmDup-xl.bed

# sum file for iCount
python ${path}scripts/BEDsum-iCount.py $path$data-STAR/$data-q255.rmDup-xl.bed $path$data-STAR/$data-q255.rmDup-xl-sum.bed

# get junction reads in bed and bam format
samtools view $path$data-STAR/$data-q255.bam | awk '($6 ~ /N/)' | samtools view -bS -t /SAN/neuroscience/TE/Shared/GRCh37.p13/GRCh37.p13.genome.fa.fai - > $path$data-STAR/$data-q255-junction-cDNAs.bam
bedtools bamtobed -i $path$data-STAR/$data-q255-junction-cDNAs.bam | awk '{print $1 "\t" $2 "\t" $3 "\t\t" $4 "\t" $6}' | sort -k1,1 -k2,2n -k5,5 -k6,6 | uniq > $path$data-STAR/$data-q255-junction-cDNAs.rmDup.bed

# print unique number of cDNAs
echo "Number of raw cDNAs (mate1 & mate2)" > $path$data-01.Mapping-REPORT.txt
$((`zcat $data | wc -l ` / 4)) >> $path$data-01.Mapping-REPORT.txt
echo "A: Unique number of (-q 255) cDNAs" >> $path$data-01.Mapping-REPORT.txt
wc -l $path$data-STAR/$data-q255.rmDup-xl.bed >> $path$data-01.Mapping-REPORT.txt
echo "B: Unique number of paired-end junction (-q 255) cDNAs" >> $path$data-01.Mapping-REPORT.txt
wc -l $path$data-STAR/$data-q255-junction-cDNAs.rmDup.bed >> $path$data-01.Mapping-REPORT.txt

# compress
gzip $path$data-STAR/*.bed
