'''
Created on Nov 28, 2013

@author: Nejc Haberman


The script will read fastq file and remove random barcode and experimental barcode from fastq. Random barcode will replace the header of a fastq file.
'''

import sys
import gzip

def swap_barcodes(fin_fastq, fout_fastq):
    with gzip.open(fin_fastq,'rb') as finFastq:
        with gzip.open(fout_fastq, 'wb') as foutFastq:
            line = finFastq.readline()
            while line:
                if line[0] == '@':
                    line = finFastq.readline()
                    randomBarcode = line[0:5] + line[11:14]
                    experimentBarcode = line[5:11]
                    www = line[14:17]	#part of the irCLIP protocol
                    seqRead = line[17:]
                    foutFastq.write("@"+randomBarcode + '\n')
                    foutFastq.write(seqRead)
                    line = finFastq.readline()
                    foutFastq.write(line)
                    line = finFastq.readline()
                    quality = line[9:]
                    foutFastq.write(quality)
                line = finFastq.readline()
            finFastq.close()
            foutFastq.close()

if sys.argv.__len__() == 3:
    fin_fastq = sys.argv[1]
    fout_fastq = sys.argv[2]
    swap_barcodes(fin_fastq, fout_fastq)
else:
    print "you need 2 arguments to run the script"
    quit()
